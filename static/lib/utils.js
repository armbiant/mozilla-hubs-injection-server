const Utils = {
	getAvatarRig:()=> {
		let rig = AFRAME.scenes[0].querySelector("#avatar-rig");
		return rig;
	},
	getHubID:()=> {
		return window.APP.hub.hub_id;
	},
	moveInFrontOfAvatar:(objectToMove, distance = 5, height=2)=> {
		let avatar = Utils.getAvatarRig();
		Utils.moveInFront(avatar, objectToMove, distance, height); 
	},
	moveInFront:(objectRef, objectToMove, distance=5, height=2)=> {
		let direction = new THREE.Vector3();
		AFRAME.scenes[0].camera.getWorldDirection( direction );
		direction.multiplyScalar(distance)
		let pos = objectRef.getAttribute("position")
		let newPos = {...{}, ...pos};
		newPos.x += direction.x
		newPos.y += height
		newPos.z += direction.z
		objectRef = null;
		objectToMove.setAttribute("position", newPos); 
	},
	removeFromScene: (objectToRemove)=> {
		objectToRemove.parentNode.removeChild(objectToRemove);
	},
	rotate: (objectToRotate, degree = 20)=> {
		let rot = objectToRotate.getAttribute("rotation");
		rot.y  += degree;
		objectToRotate.setAttribute("rotation", rot);
	},
	move: (objectToMove, x=0, y=x, z=y)=> {
		let pos = objectToMove.getAttribute("position");
		//var newPos = {...{}, ...pos};
		console.log(pos);
		pos.x += x
		pos.y += y
		pos.z += z
		console.log(pos);
		objectRef = null;
		objectToMove.setAttribute("position", pos); 
	},
	resize: (objectToResize, x=0, y=x, z=y)=> {
		objectToResize.object3D.scale.set(x, y, z);
	},
	getPlayerList: ()=> {
		return document.querySelectorAll("a-entity[player-info]");
	},
	getPresenceList:()=> {
		return window.APP.hubChannel.presence.list();
	},
	getNumOccupants:()=> {
		return Object.keys(NAF.connection.adapter.occupants).length;
	},
	getPlayer: ()=> {
		return AFRAME.scenes[0].systems["hubs-systems"].characterController;
	},
	postData:(url, data)=> {
		return new Promise((resolve, reject) => {
			try {
				let saveData = async ()=> {
					let response = await fetch(url, {
						method: 'POST',
						headers: 
						{
							'Accept': 'application/json',
							'Content-Type': 'application/json'
							// 'Content-Type': 'application/x-www-form-urlencoded',
						},
						mode: 'cors',
						body: JSON.stringify(data) // body data type must match "Content-Type" header
					});
					await response.json();
					return resolve(response); 
				};
				saveData();
			}
			catch(err) {
				return reject(err);
			};
		});
	},
	store: {
		save:(hubsStorableType, objectID, data)=> {
			return new Promise((resolve, reject)=> {
				let hubID = window.APP.hub.hub_id;
				try {
					let saveData = async ()=> {
						switch (hubsStorableType) {
							case "component":
							let dataObj = {'hubID':hubID, 'componentID':objectID, 'configs':data};
							let componentData = await SAT.Utils.postData(`${SAT.baseURI}/hubs/component`, dataObj);
							return resolve(componentData);
							break;
		
							case "room":
							break;
		
							case "avatar":
							break;
						}
					}
					saveData();					
				}
				catch(err) {
					return reject(err);
				}
			})
		},
		find:(hubsStorableType, objectID)=> {
			return new Promise((resolve, reject)=> {
				try {
					let doQuery = async()=> {
						let hubID = window.APP.hub.hub_id;
						switch (hubsStorableType) {
							case "component":
							const response = await fetch(`${SAT.baseURI}/hubs/component/${hubID}/${objectID}`);
							if (response.ok) {
								let json = await response.json();
								return resolve(json);
							} else {
								return reject("Not OK");
							}
							break;

							case "room":
							break;

							case "avatar":
							break;
						}
					}
					doQuery();
				}
				catch(err) { 
					return reject(); 
				}
			});
		},
		query:(hubsStorableType, queryObj)=> {
			return new Promise((resolve, reject)=> {
				switch (hubsStorableType) {
					case "component":
					break;

					case "room":
					break;

					case "avatar":
					break;
				}
			})
		}
	},
	io: {
		room: {
			register:()=> {
				SAT.__hubsRoomsIO__.emit('register', {'hubsID':window.APP.hub.hub_id});
			},
			emit:(data)=> {
				const hubsID = data.hubsID != null ? data.hubsID : window.APP.hub.hub_id;
				let broadcast = {'hubsID':hubsID};
				SAT.__hubsRoomsIO__.emit("sendToRoom", {...broadcast, ...data});
			},
			emitToRooms:(hubsIDs, data)=> {
				let broadcast = {'hubsIDs':hubsIDs};
				SAT.__hubsRoomsIO__.emit("sendToRooms", {...broadcast, ...data});
			},
			emitToAllRooms:(data)=> {
				SAT.__hubsRoomsIO__.emit("sendToAllRooms", data);
			},
			on:(msgType, cb)=> {
				SAT.__hubsRoomsIO__.on(msgType, cb);
			}
		},
		component: {
			register:(componentID)=> {
				SAT.__componentsIO__.emit('register', {'hubsID':window.APP.hub.hub_id, 'componentID':componentID});
			},
			emit:(componentID, data)=> {
				const hubsID = data.hubsID != null ? data.hubsID : window.APP.hub.hub_id;
				let broadcast = {'hubsID':hubsID, 'componentID':componentID};
				SAT.__componentsIO__.emit("sendToRoomComponent", {...broadcast, ...data});
			},
			emitToRoomComponents:(componentIDs, data)=> {
				const hubsID = data.hubsID != null ? data.hubsID : window.APP.hub.hub_id;
				let broadcast = {'hubsID':hubsID, 'componentIDs':componentIDs};
				SAT.__componentsIO__.emit("sendToRoomComponents", {...broadcast, ...data});
			},
			emitToComponentAcrossRooms:(componentID, data)=> {
				let broadcast = {'componentID':componentID};
				SAT.__componentsIO__.emit("sendToComponentAcrossRooms", {...broadcast, ...data});			
			},
			emitToAllComponents:(data)=> {
				SAT.__componentsIO__.emit("sendToAllComponents", data);
			},
			on:(msgType, cb)=> {
				SAT.__componentsIO__.on(msgType, cb);
			}
		},
		avatar: { }
	}
}

SAT.Utils = Utils;

window.loadSocketIO = function() {
	SAT.__io__ = io(SAT.baseURI, {
		withCredentials: true,
		extraHeaders: { "hubs-header": "yourHubsHeaderKey" },
		transports: ['websocket']
	});

	SAT.__io__.on("connect", () => {
		console.log("connected to IO")
	});

	SAT.__hubsRoomsIO__ = io(`${SAT.baseURI}/hubsRooms`, {
		withCredentials: true,
		extraHeaders: { "hubs-header": "yourHubsHeaderKey" },
		transports: ['websocket']
	});

	SAT.__hubsRoomsIO__.on("connect",()=> {
		
	});

	SAT.__componentsIO__ = io(`${SAT.baseURI}/components`, {
		withCredentials: true,
		extraHeaders: { "hubs-header": "yourHubsHeaderKey" },
		transports: ['websocket']
	});

	SAT.__componentsIO__.on("connect",()=> {
		// console.log("connected to componentsIO");
	});

	SAT.__avatarsIO__ = io(`${SAT.baseURI}/avatars`, {
		withCredentials: true,
		extraHeaders: { "hubs-header": "yourHubsHeaderKey" },
		transports: ['websocket']
	});

	SAT.__avatarsIO__.on("connect",()=> {
		// console.log("connected to avatarsIO")
	});
}


var socketIO = document.createElement('script');
socketIO.onload = function() { window.loadSocketIO(); };
  
socketIO.setAttribute('src',`${SAT.baseURI}/lib/socket.io.js`);   // https://api.satlit.sat.qc.ca/lib/socket.io.js 
document.head.appendChild(socketIO);

document.body.addEventListener('connected', (evt) => {
	SAT.Utils.io.room.register();
	SAT.Utils.io.room.on('IOMessage', (data)=> {
		console.log("received room message", data);
	})
	setTimeout(()=> {
		SAT.Utils.io.room.emit({msg:"HI from room"});
	}, 
	1000);
	console.log('IO connected event. clientId =', evt.detail.clientId);
});