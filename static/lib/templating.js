const vectorRequiresUpdate = epsilon => {
    return () => 
    {
      let prev = null;

        return curr => 
        {
            if (prev === null) 
            {
                prev = new THREE.Vector3(curr.x, curr.y, curr.z);
                return true;
            }    
            else if (!NAF.utils.almostEqualVec3(prev, curr, epsilon)) 
            {
                prev.copy(curr);
                return true;
            }

            return false;
        };
    };
};

class Templating 
{
    constructor() 
    { 
        this.buildSimpleComponent();
        this.injectSimpleTemplate("primitive-media", "primitivecomp");
    }

    injectSimpleTemplate(id, component="", entity="")
    {
        let assets = document.querySelector("a-assets");
        let newTemplate = document.createElement("template");
        newTemplate.id = "primitive-media";
        newTemplate.innerHTML = `<a-entity 
        class="interactable" 
        owned-object-limiter="counter: #media-counter" 
        is-remote-hover-target 
        tags="singleActionButton:true; isHandCollisionTarget: true; isHoldable: true; offersHandConstraint: true; offersRemoteConstraint: true; inspectable: true;" 
        destroy-at-extreme-distances 
        floaty-object="modifyGravityOnRelease: false; autoLockOnLoad: false;" 
        set-yxz-order 
        set-unowned-body-kinematic 
        matrix-auto-update 
        hoverable-visuals
        ${component}
        >
        ${entity}
        </a-entity>`
        assets.appendChild(newTemplate);
        NAF.schemas.add({
            template: "#primitive-media",
            components: [
                {
                    component: "position",
                    requiresNetworkUpdate: vectorRequiresUpdate(0.001)
                },
                {
                    component: "rotation",
                    requiresNetworkUpdate: vectorRequiresUpdate(0.5)
                },
                {
                    component: "scale",
                    requiresNetworkUpdate: vectorRequiresUpdate(0.001)
                },
                {
                    component: "body-helper"
                },
                'media-loader',
                "pinnable"
            ]
        }); 
    }

    injectInteractiveTemplate(id, component="", entity="", emit="")
    {
        let emitStr = emit !=''?`="${emit}"`:'';
        let assets = document.querySelector("a-assets");
        let newTemplate = document.createElement("template");
        newTemplate.id = `${id}`;
        newTemplate.innerHTML = `<a-entity 
        class="interactable" 
        body-helper="type: dynamic; mass: 1; collisionFilterGroup: 1; collisionFilterMask: 15;" 
        owned-object-limiter="counter: #media-counter" 
        set-unowned-body-kinematic 
        is-remote-hover-target 
        tags="isHandCollisionTarget: true; isHoldable: true; offersHandConstraint: true; offersRemoteConstraint: true; inspectable: true;" 
        destroy-at-extreme-distances 
        scalable-when-grabbed 
        floaty-object="modifyGravityOnRelease: true; autoLockOnLoad: true;" 
        set-yxz-order 
        matrix-auto-update 
        hoverable-visuals
        ${component}${emitStr}>
        ${entity}
        </a-entity>`
        assets.appendChild(newTemplate);
        NAF.schemas.add({
            // template to add (created above)
            template: `#${id}`,
            components: [
                {
                    component: "position",
                    requiresNetworkUpdate: vectorRequiresUpdate(0.001)
                },
                {
                    component: "rotation",
                    requiresNetworkUpdate: vectorRequiresUpdate(0.5)
                },
                {
                    component: "scale",
                    requiresNetworkUpdate: vectorRequiresUpdate(0.001)
                },
                {
                    component: "color",
                    requiresNetworkUpdate: vectorRequiresUpdate(0.001)
                },
                "pinnable"
            ]
        }); 

    }

    buildSimpleComponent()
    {
        AFRAME.registerComponent('primitivecomp', 
        {
            schema: 
            {
                emitclick: { type: 'string' },
                emitmouseover: { type: 'string' },
                emitmouseout: { type: 'string' }
            },
            init() 
            {
                let data = this.data;
                let el = this.el;
                el.bypassPostClick = false;
                el.clicked = false;
                el.mousedOver2 = false;
                el.mousedOver1 = false;
                NAF.utils.getNetworkedEntity(this.el)
                    .then(networkedEl => 
                    {
                        this.networkedEl = networkedEl;
                        networkedEl.id = 'networked_'+el.id;
                        this.networkedEl.object3D.addEventListener("interact", ()=> 
                        { 
                            if (this.networkedEl && !NAF.utils.isMine(this.networkedEl))
                            {
                                NAF.utils.takeOwnership(this.networkedEl);
                            }
                            el.clicked = true;
                            this.networkedEl.emit(data.emitclick, {"clicked":true}); 
                        });
                     })
                    .catch(() => { console.log("Is not networked ?")});
               
            },
            tick(time) 
            {
                let data = this.data;
                const isFrozen = this.el.sceneEl.is("frozen");
             
                const toggling = this.el.sceneEl.systems["hubs-systems"].cursorTogglingSystem;
          
                let interactorOne, interactorTwo;
                const interaction = this.el.sceneEl.systems.interaction;
                if (!interaction.ready) return; //DOMContentReady workaround
                if (interaction.state.leftHand.hovered === this.el && !interaction.state.leftHand.held) 
                {
                    interactorOne = interaction.options.leftHand.entity.object3D;
                }

                if (interaction.state.leftRemote.hovered === this.el &&
                    !interaction.state.leftRemote.held &&
                    !toggling.leftToggledOff) 
                {
                    interactorOne = interaction.options.leftRemote.entity.object3D;
                }

                if (interaction.state.rightRemote.hovered === this.el &&
                    !interaction.state.rightRemote.held &&
                    !toggling.rightToggledOff) 
                {
                    interactorTwo = interaction.options.rightRemote.entity.object3D;
                }
                if (interaction.state.rightHand.hovered === this.el && !interaction.state.rightHand.held) 
                {
                    interactorTwo = interaction.options.rightHand.entity.object3D;
                }
          
                if (interactorOne) 
                {
                    if (!this.el.mousedOver1)
                    {
                        this.el.mousedOver1 = true;
                        // console.log("interactor one hover")
                        NAF.utils.getNetworkedEntity(this.el)
                            .then(networkedEl => 
                            {
                                console.log("mousedover in networked element");
                                this.networkedEl = networkedEl;
                                this.networkedEl.emit(data.emitmouseover, {"interactor":1}); 
                            })
                            .catch(err => { });
                    }
                }
                else
                {
                    if (this.el.mousedOver1)
                    {
                        this.el.mousedOver1 = false;
                        // console.log("intercator one hovered out")
                        NAF.utils.getNetworkedEntity(this.el)
                            .then(networkedEl => 
                            {
                                console.log("mousedout in networked element");
                                this.networkedEl = networkedEl;
                                this.networkedEl.emit(data.emitmouseout, {"interactor":1}); 
                            })
                            .catch(err => { });
                    }
                }
                if (interactorTwo) 
                {
                    if (!this.el.mousedOver2)
                    {
                        if (!this.el.clicked)
                        {
                            this.el.mousedOver2 = true;
                            // console.log("intercator two hover")
                            NAF.utils.getNetworkedEntity(this.el)
                                .then(networkedEl => 
                                {
                                    console.log("mousedover in networked element");
                                    this.networkedEl = networkedEl;
                                    this.networkedEl.emit(data.emitmouseover, {"interactor":2}); 
                                })
                                .catch(err => { });
                        }
                        else
                        {
                            if (this.el.bypassPostClick)
                            {
                                this.el.clicked = false;
                                this.el.mousedOver2 = true;
                                this.el.bypassPostClick = false;
                            }
                        }
                    }
                }
                else
                {
                    if (this.el.mousedOver2)
                    {
                        if (!this.el.clicked)
                        {
                            this.el.mousedOver2 = false;
                            // console.log("intercator two hovered out")
                            NAF.utils.getNetworkedEntity(this.el)
                                .then(networkedEl => 
                                {
                                    console.log("mousedout in networked element");
                                    this.networkedEl = networkedEl;
                                    this.networkedEl.emit(data.emitmouseout, {"interactor":2}); 
                                })
                                .catch(err => { });
                        }
                        else
                        {
                            this.el.mousedOver2 = false;
                            this.el.bypassPostClick = true;
                        }
                    }
                }
          
                if (interactorOne || interactorTwo || isFrozen) 
                {
                        // --> Dont do anything or event should either stop bubbling... have to think about this one.      
                }
            }
        });
    }
}


if (!SAT)
{
    var SAT = {templating:new Templating()};
}
else
{
    SAT.templating = new Templating();
}


