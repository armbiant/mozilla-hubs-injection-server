const http              = require("http")
    , https             = require("https")
    , express           = require("express")
    , HubsRoutes        = require('./routers/HubsRoutes')
    , path              = require('path')
    , cors              = require('cors')
    , cookieParser      = require('cookie-parser')
    , fs                = require('fs').promises
    , mongoose          = require('mongoose')
    , selfsigned        = require('selfsigned');

    require('dotenv').config();

class HubsInjectionServer
{
    app                = null;
    httpServer         = null;
    hubsRoutes         = new HubsRoutes({}, this);
    appListener        = null;
    whitelist           = ['http://localhost:8080', 'http://localhost', 'https://hubs.local:8080', 'https://hubs.local'];
    
    constructor(configs) {
        mongoose.set('useFindAndModify', false);

        if (this.whitelist.indexOf(process.env.HUB_URI) < 0)
        {
            this.whitelist.push(process.env.HUB_URI);
        }

        let corsOptions = {
            origin: (origin, callback) => {
                console.log(origin);
                if (this.whitelist.indexOf(origin) !== -1) {
                callback(null, true);
              } else {
                callback(null, false);
              }
            },
            methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
            optionsSuccessStatus: 204, // some legacy browsers (IE11, various SmartTVs) choke on 204 -->> check that
            credentials: true, //Credentials are cookies, authorization headers or TLS client certificates.
            allowedHeaders: ['hubs-header', 'Content-Type', 'Authorization', 'X-Requested-With', 'device-remember-token', 'Access-Control-Allow-Origin', 'Origin', 'Accept']
        };

        this.app = express();
        this.app.use(cookieParser())
        this.app.use(cors(corsOptions));
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(express.json());

        if (process.env.USE_LOCAL_SSL_CERTIFICATES && process.env.USE_LOCAL_SSL_CERTIFICATES == 1)
        {
            const fsSync = require('fs');
            const filesExist = fsSync.existsSync(path.join(__dirname, "certs"));
            console.log("filesExist", filesExist);
            let key;
            let cert;
            
            if (process.env.REGEN_LOCAL_SSL_CERTIFICATES == 1 || !filesExist)
            {
                const pems = selfsigned.generate([
                    { name: "commonName", value: `"${process.env.LOCAL_HOSTNAME}"`}
                ], 
                {
                    days: 365, algorithm: "sha256",
                    extensions: [ {
                        name: "subjectAltName",
                        altNames: [ 
                            { type: 2, value: "localhost" },
                            { type: 2, value: `"${process.env.LOCAL_HOSTNAME}"`}
                            ]
                        }
                    ]
                });
              
                fsSync.mkdirSync(path.join(__dirname, "certs"));
                fsSync.writeFileSync(path.join(__dirname, "certs", "cert.pem"), pems.cert);
                fsSync.writeFileSync(path.join(__dirname, "certs", "key.pem"), pems.private);      
                key = pems.private,
                cert = pems.cert         
            }
            else if (filesExist) {
                key = fsSync.readFileSync(path.join(__dirname, "certs", "key.pem"));
                cert = fsSync.readFileSync(path.join(__dirname, "certs", "cert.pem"));
            }

            const options = { key: key, cert: cert, requestCert: false, rejectUnauthorized: false };
            this.httpServer = require('https').createServer(options, this.app);
        }
        else
        {
            this.httpServer = require('http').createServer(this.app);
        }
        
        this.app.use(express.static(path.join(__dirname, 'static')));
        this.app.use('/hubs', this.hubsRoutes.router);

        let basePath = process.cwd();
        
        this.doFileGeneration(`${basePath}/templates/satutils.ktl`, {baseURI:process.env.BASE_URI}, `${basePath}/static/lib/satutils.js`);

        this.appListener = this.httpServer.listen(process.env.API_PORT||8557, () => {
           console.log(`Running Mozilla Hubs Injection Layer on port ${process.env.API_PORT||8557}`)
        });

        this.io = require('socket.io')(this.httpServer);

        this.dbConn = mongoose.connect(process.env.DB_HOST, { useUnifiedTopology: true, useNewUrlParser: true }, ()=> {
            if (this.resetStorage) {
                const doQueue = async()=> {
                    await this.resetDatabase();
                };
                doQueue();
            }
        });
        this.mongoDB = mongoose.connection;

        this.socketIOConnected = false;
        this.waitingOnSocketConnection = false;
        this.socketIOClientCount = 0;
        this.__initSocketIO();
    }

    __initSocketIO() {
        if (this.io) {
            this.socketIOConnected = true;
            this.waitingOnSocketConnection = true;
            this.io.of("/hubsRooms").on('connection', socket => {
                socket.on('register', (data) => {
                    if (data.hubsID != null) {
                        socket.join(data.hubsID);
                    }
                });

                socket.on('sendToRoom', (data) => {   
                    if (data.hubsID != null) {
                        const msgType = data.msgType != null ? data.msgType : "IOMessage";
                        this.io.of("/hubsRooms").to(data.hubsID).emit(msgType, data.msg);
                    }
                });

                socket.on('sendToRooms', (data) => {   
                    if (data.hubsIDs != null && data.hubsIDs.length > 0) {
                        const msgType = data.msgType != null ? data.msgType : "IOMessage";
                        for (let i=0; i<data.hubsIDs.length; ++i) {
                            this.io.of("/hubsRooms").to(data.hubsIDs[i]).emit(msgType, data.msg);
                        }
                    }
                });

                socket.on('sendToAllRooms', (data) => {   
                    const msgType = data.msgType != null ? data.msgType : "IOMessage";
                    this.io.emit(msgType, data.msg);
                });
            });

         
            this.io.of("/components").on('connection', socket => {
                socket.on('register', (data) => {
                    if (data.componentID != null) {
                        socket.join(data.componentID);
                        if (data.hubsID != null) {
                            socket.join(`${data.hubsID}_${data.componentID}`);
                        }
                    }
                });

                socket.on('sendToRoomComponent', (data) => {   
                    if (data.componentID != null && data.hubsID != null) {
                        const msgType = data.msgType != null ? data.msgType : "IOMessage";
                        const channel = `${data.hubsID}_${data.componentID}`;
                        this.io.of("/components").to(channel).emit(msgType, data.msg);
                    }
                });

                socket.on('sendToRoomComponents', (data) => {   
                    if (data.componentIDs != null && data.componentIDs.length > 0 && data.hubsID != null)
                    {
                        for (let i=0; i<data.componentIDs.length; ++i)
                        {
                            const msgType = data.msgType != null ? data.msgType : "IOMessage";
                            this.io.of("/components").to(`${data.hubsID}_${data.componentIDs[i]}`).emit(msgType, data.msg);
                        }
                    }
                });

                socket.on('sendToComponentAcrossRooms', (data) => {   
                    if (data.componentID != null) {
                        const msgType = data.msgType != null ? data.msgType : "IOMessage";
                        this.io.of("/components").to(data.hubsID).emit(msgType, data.msg);
                    }
                });

                socket.on('sendToAllComponents', (data) => {  
                    const msgType = data.msgType != null ? data.msgType : "IOMessage";
                    this.io.emit(msgType, data.msg);
                });

            });

            this.io.on('connection', (client) => {
                if (client) {
                    let clientIP = client.request.connection.remoteAddress.split("::ffff:")[1];
                    this.socketIOClientCount ++;
                    this.waitingOnSocketConnection = false;
                    
                    client.on('disconnect', ()=> {
                        if (this.socketIOClientCount > 0) {
                            this.socketIOClientCount --;
                        }
                        
                        /*
                        let clientIP = client.request.connection.remoteAddress.split("::ffff:")[1];
                        
                        if (this.socketIOClientCount == 0) {
                            console.log("No clients are connected to socket");
                        } */
                    });
                }
            });

            this.io.on('join', (data) => {
                // console.log("joined", data);
            });
        }
    }

    
    doFileGeneration(templateFilePath, variables, outputLocation) 
    {
        let doGenerate = async()=> {
            let basePath = process.cwd();
            let fileContent = await this.renderFile(templateFilePath, variables);
            await this.fsWriteFile(outputLocation, fileContent); 
        }
        doGenerate();
    }

    fsWriteFile(path, fromData) {
        return new Promise((resolve, reject) => 
        {
            fs.writeFile(path, fromData, (err, data) => {
                if (err) reject(err);
                resolve(data);
              });
        })
    }

    render(templateString, templateVariables) {
        const rendered = templateString.replace(/\${(.*?)}/g, (_, g) => templateVariables[g]);
        return rendered;    
    }

    getTemplatedFile(path) {
        return new Promise((resolve, reject) => {
            const doFileRead = async()=> {
                try {
                    let file = await fs.readFile(path, 'utf-8');
                    return resolve(file);
                }
                catch(err) {
                    return reject(err);
                }
            };
            doFileRead();
        });
    }
 
    renderFile(filePath, templateVariables) {
        return new Promise((resolve, reject) => {
            const doReadAndRender = async()=> {
                let fileAsString = await this.getTemplatedFile(filePath);
                return resolve(this.render(fileAsString, templateVariables));
            };
            doReadAndRender();
        });
    }
}

let server = new HubsInjectionServer();
