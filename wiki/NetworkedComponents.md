# 6. How to share components on the network.  
Up untill this point we have created a rectangle component that is displayed for every avatar entering the room. 
But what happens if we want to not only display a component for everyone in the room, but to change some of it's properties and have it reflect for every user in the room in realtime... like most components you see in MozillaHubs. That's what we will show you by adding a few pieces of code to our rectangleComponent.

A key concept for persistent networked components to grasp in NAF (Networked A-Frame) is that components have to first be created on every connected client, and then NAF allows us to synch up component properties in real time. 

If you are like us, you might think of creating a networked component on one client and, since it's networked, all of the clients will automatically create it.... well NO, that's not the case. 


**STEP 1: CREATING THE NETWORKED COMPONENT FILES**.  

- In the Injection Server project, go to static/hubs-components and create a new folder. Call it `networkedRectangle`. Inside that folder, create a javascript file called `NetworkedRectangleComponent.js`. 

- Then, go to your room `configs.js` (could be in the generic room) and link up `NetworkedRectangleComponent.js` as such:  
```
{
    "modules": {
        "client":
        [
            {"id":"satutils", "url":"/lib/satutils.js"},
            {"id":"utils", "url":"/lib/utils.js"},
            {"id":"networkedRectangleExemple", "url":"/hubs-components/networkedRectangle/NetworkedRectangleComponent.js"}
        ],
        "server":
        [
            
        ]
    }  
}
``` 
- Save all of those. 

**STEP 2: CREATING THE NETWORKED COMPONENT**.  

- Copy and paste the following inside `NetworkedRectangleComponent.js`.

```
class NetworkedRectangleComponent {
    constructor(rectangleID, rectangleConfigs) {
        this.rectangleID = rectangleID;
        this.defaultConfigs =  {
            text_string:"DEFAULT TEXT",
            rectangle_width:2.5,
            rectangle_height:0.4,
            rectangle_color:'#550000',
            rectangle_position:'0 2 0',
            text_index:0,
            word_list:'famous|drip|uncle|awake|potato|jagged|secretary|license|stretch|shelf|explain|lie'
        }

        this.configs = {...this.defaultConfigs, ...rectangleConfigs};
        this.registerComponent(this.configs);
        this.buildTemplate("networkedrectangle-media");
        this.createEntity();
    }

    registerComponent(configs) {
        AFRAME.registerComponent('networkedrectanglecomponent', {
            schema: {
                rectangle_text_string: { type:'string', default:configs.text_string },
                rectangle_color: { type: 'color', default: configs.rectangle_color },
                rectangle_id:{type:'string', default: this.rectangleID},
                rectangle_width:{type:'number', default: configs.rectangle_width },
                rectangle_height:{type:'number', default: configs.rectangle_height },
                rectangle_position:{type:'string', default:configs.rectangle_position},
                text_index:{type:'number', default: configs.text_index },
                word_list:{type:'string', default:configs.word_list}
            },
            init:function() {
                let data = this.data;
                let el = this.el;
                this.update = this.update.bind(this);
                this.geometry = new THREE.PlaneBufferGeometry(data.rectangle_width, data.rectangle_height, 1, 1);
                this.material = new THREE.MeshBasicMaterial({ color: data.rectangle_color });
                this.material.transparent = true;
                this.material.opacity = 0.8;
                this.material.side = THREE.DoubleSide;
                
                // Create mesh.
                this.mesh = new THREE.Mesh(this.geometry, this.material);
                // Set mesh on entity.
                
                el.setObject3D('mesh', this.mesh);
                el.setAttribute('position', `${data.rectangle_position}`);
                
                NAF.utils
                    .getNetworkedEntity(this.el)
                    .then(networkedEl => {
                            
                        this.networkedEl = networkedEl;
                        this.networkedEl.setAttribute('text', 'value', `${data.rectangle_text_string}`);
                        this.networkedEl.setAttribute('text', 'align', 'center');
                        this.networkedEl.setAttribute('text', 'transparent', 'false');
                        this.networkedEl.setAttribute('text', 'wrapCount', 30);
                        this.networkedEl.setAttribute('text', 'width', 2.5);

                        this.networkedEl.object3D.addEventListener('interact', ()=> {
                            this.selectedRect(this.data, this.el);
                        });
                    });
            },
            update:function(oldData)
            {
                let doUpdate = async()=>
                {
                    let networkedEl = await NAF.utils.getNetworkedEntity(this.el);
                    this.networkedEl = networkedEl;
                    this.networkedEl.setAttribute('text', 'value', `${this.data.text_string}`);
                    this.networkedEl.setAttribute('text', 'align', 'center');
                    this.networkedEl.setAttribute('text', 'transparent', 'false');
                    this.networkedEl.setAttribute('text', 'wrapCount', 30);
                    this.networkedEl.setAttribute('text', 'width', 2.5);
                }
                doUpdate();
            },
            selectedRect:function(data, el)
            {
                let doSelection = async()=>
                {
                    let rectNetworkElement = await NAF.utils.getNetworkedEntity(el);
                    if (!NAF.utils.isMine(rectNetworkElement))
                    {
                        NAF.utils.takeOwnership(rectNetworkElement);
                    }
                    let wordList = data.word_list.split("|");
                    let wordIndex = Number(data.text_index);
                    let word;
                    if (wordIndex < wordList.length-1)
                    {
                        rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_index', wordIndex+1);
                        word = wordList[wordIndex+1];
                        rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_string', word);
                    }
                    else
                    {
                        rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_index', 0);
                        word = wordList[0];
                        rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_string', word);
                    }
                     
                }
                doSelection();
            }
        });
    }

    buildTemplate(templateID) {
        let assets = document.querySelector("a-assets");
        let newTemplate = document.createElement("template");
        
        newTemplate.id = `${templateID}`;
        newTemplate.innerHTML = `<a-entity 
        class="interactable" 
        is-remote-hover-target 
        set-unowned-body-kinematic 
        tags="isHandCollisionTarget: false; isHoldable: false; offersHandConstraint: false; offersRemoteConstraint: false; inspectable: true; singleActionButton:true; isStatic: true;togglesHoveredActionSet: true;" 
        body-helper="type: static; mass: 1; collisionFilterGroup: 1; collisionFilterMask: 15;"
        hoverable-visuals 
        matrix-auto-update
        pinnable
        networkedrectanglecomponent
        >
        </a-entity>`;
        assets.appendChild(newTemplate);
        NAF.schemas.add({
            // template to add (created above)
            template: `#${templateID}`,
            components: [{
                    component: "position"
                },
                {
                    component: "rotation"
                },
                {
                    component: "scale"
                },
                "networkedrectanglecomponent", 
                {
                    component: "networkedrectanglecomponent",
                    attribute:"text_string"
                }, 
                {
                    component: "networkedrectanglecomponent",
                    attribute:"text_index"
                }
            ]
        });
    }

    createEntity() {
        return new Promise((resolve, reject) => { 
            let doCreate = async () => { 
                let rectangleEntity = document.createElement("a-entity");
                let loadedRectangle = new Promise((resolve, reject) => { rectangleEntity.addEventListener('loaded', resolve, { once: true }) });
                rectangleEntity.setAttribute("id", `${this.rectangleID}Entity`);
                rectangleEntity.setAttribute("networkedrectanglecomponent", `rectangle_id:${this.rectangleID};`);
                rectangleEntity.setAttribute('networked', {template: "#networkedrectangle-media", "persistent":true, "attachTemplateToLocal":true, "networkId":`${this.rectangleID}_netRect}` });
                document.querySelector('a-scene').appendChild(rectangleEntity);
                await loadedRectangle;
                
                return resolve(loadedRectangle);
            }
            doCreate();
        });
    }
}

let rectangle;
setTimeout(()=> {
    rectangle = new NetworkedRectangleComponent("rectangle1", null);
}, 
1000);
```

**STEP 3: WHAT CHANGED ?**.  
Here is what we changed in this version compared to our earlier, not networked, and not interactive component. 

- Added 2 new config variables to our class that will get passed down to our component attributes. Those two are for interactive purposes, our exemple now consists of a clickable rectangle. When one user clicks, the text value updates for every user in real time. The variables are basically there to build the logic so that each clicks generates a cycling action through a list of words. 
``` 
text_index:0,
word_list:'famous|drip|uncle|awake|potato|jagged|secretary|license|stretch|shelf|explain|lie'
```  
- Changed the template name to "networkedrectangle-media" as in: `this.buildTemplate("networkedrectangle-media");`
- Changed the component name to "networkedrectanglecomponent" as in: `AFRAME.registerComponent('networkedrectanglecomponent', `
- Added two attributes to the component for the interaction logic: 
```
text_index:{type:'number', default: configs.text_index },
word_list:{type:'string', default:configs.word_list}
```
- Linked up the text entity on the actual networked element and added a listener to the interact event of the networked element (like a mouse 'click') and inserted a callback to a method included in the component schema (selectedRect). 
```
NAF.utils
    .getNetworkedEntity(this.el)
    .then(networkedEl => {
        this.networkedEl = networkedEl;
        this.networkedEl.setAttribute('text', 'value', `${data.rectangle_text_string}`);
        this.networkedEl.setAttribute('text', 'align', 'center');
        this.networkedEl.setAttribute('text', 'transparent', 'false');
        this.networkedEl.setAttribute('text', 'wrapCount', 30);
        this.networkedEl.setAttribute('text', 'width', 2.5);

        this.networkedEl.object3D.addEventListener('interact', ()=> {
            this.selectedRect(this.data, this.el);
        });
    });

```
- Added implementation for the NAF update method. Update is what gets called anytime any attribute's data is modified. In update we can compare between oldData and newData which is quite practical for many reasons, for exemple we might want to segregate locic tiers for particular attributes. In our case we are not placing any care to what value gets updated, but in many cases you will need this. Basicaly, think of it as: oldData contains the data values for your attributes before the update was called, and this.data (inside of the update method scope) represents your new data set.   
```
update:function(oldData) {
    let doUpdate = async()=>
    {
        let networkedEl = await NAF.utils.getNetworkedEntity(this.el);
        this.networkedEl = networkedEl;
        this.networkedEl.setAttribute('text', 'value', `${this.data.text_string}`);
        this.networkedEl.setAttribute('text', 'align', 'center');
        this.networkedEl.setAttribute('text', 'transparent', 'false');
        this.networkedEl.setAttribute('text', 'wrapCount', 30);
        this.networkedEl.setAttribute('text', 'width', 2.5);
    }
    doUpdate();  
},
```
- Added a selectedRect method to the schema to hold logic for the interaction. 
```
selectedRect:function(data, el)
{
    let doSelection = async()=>
    {
        let rectNetworkElement = await NAF.utils.getNetworkedEntity(el);
        if (!NAF.utils.isMine(rectNetworkElement))
        {
            NAF.utils.takeOwnership(rectNetworkElement);
        }
        let wordList = data.word_list.split("|");
        let wordIndex = Number(data.text_index);
        let word;
        if (wordIndex < wordList.length-1)
        {
            rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_index', wordIndex+1);
            word = wordList[wordIndex+1];
            rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_string', word);
        }
        else
        {
            rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_index', 0);
            word = wordList[0];
            rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_string', word);
        }
            
    }
    doSelection();
}
```
Into the selectedRect method we first fetch our networkedComponent:  `let rectNetworkElement = await NAF.utils.getNetworkedEntity(el);`
Then we take onwership of the component if it is not already ours. `NAF.utils.takeOwnership(rectNetworkElement);`  
Always take ownership of a networked component before doing changes to it's attributes.  

Then we have logic for cycling through the wordList which is a no brainer. The only thing that is important for us is how we update the attributes. 

`rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_index', wordIndex+1);`
and
`rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_string', word);`

Setting those attributes will trigger the NAF update method implemented into the schema on all clients (if networked properly). 
The new data set fetched inside will now contain a new value for wordIndex and for word. The same value will get distributed amongst connected clients.  

- Updated the component name at template creation with proper component name: 
```
... 
    matrix-auto-update
    pinnable
    networkedrectanglecomponent
    >
</a-entity>`;
...
```

- Added component attributes to share over the network in `NAF.schemas.add()`

```
NAF.schemas.add({
        // template to add (created above)
        template: `#${templateID}`,
        components: [{
                component: "position"
            },
            {
                component: "rotation"
            },
            {
                component: "scale"
            },
            "networkedrectanglecomponent", 
            {
                component: "networkedrectanglecomponent",
                attribute:"text_string"
            }, 
            {
                component: "networkedrectanglecomponent",
                attribute:"text_index"
            }
        ]
    });
```
This portion might need some more work to make it cleaner, but this is how we get things to work on our end.  
To the component list, add the `networkedrectanglecomponent` on a single line: `"networkedrectanglecomponent",`
Then add it's component networked attributes in an object with two keys: component and attributes. As in the two following lines: 
```
{
    component: "networkedrectanglecomponent",
    attribute:"text_string"
}, 
{
    component: "networkedrectanglecomponent",
    attribute:"text_index"
}
```
We do so for each attributes we want to sync over the network.  

- Updated the component name in the entity creation logic.
`rectangleEntity.setAttribute("networkedrectanglecomponent", `rectangle_id:${this.rectangleID};`);`  
- Added the networked attribute to the HTML entity with options.  
```
rectangleEntity.setAttribute('networked', {template: "#networkedrectangle-media", "persistent":true, "attachTemplateToLocal":true, "networkId":`${this.rectangleID}_netRect}` });
```

This line glues all the network logic together for NAF.  
Set the `template` key to contain your **template name**.  
Set the `persistent` key to **true** to make in Network Persistent and allow automatic ownership leasing if the client that currently owns the component suddenly leaves the room. This makes it so ownership is passed automatically to another client.  
Set the `attachTemplateToLocal` to **true**. Up to this point we still dont understand how that works completely, but we always set it to true.  
  
And then the 'PIECE DE RESISTANCE':  
Set the `networkId` to a static value, unique for each component instance you create but it has to **BE THE SAME FOR EVERY CLIENT.**  

## TEST THE EXEMPLE  
Open up your Hubs room on two different browsers, or the same but one in incognito mode and one in regular mode.  
Clicking the red rectangle should update the text inside the rectangle on both the client where the click occurs and the other connected client. Try it both ways. 

**NICE !!** 

| Previous Page | Home | Next Page |
| ------ | ------ | ------ |
| [How to create injected components for Mozilla Hubs.](./HowToCreateInjectedComponent.md) | [index](../README.md) | [How to save states and values to a database.](./SavingToDB.md) |