# 2. Setting up the Mozilla Hubs Custom Client in order to fetch code.  

In order to inject code at runtime in a Mozilla Hubs client, we need to get the Injection Server and the client talking to each other.  

The way we have accomplished this is by adding a small fetch method to the client that calls the Injection Server at a specific route along with the current Hubs roomID. Upon reception of this call, the Injection Server delivers the proper javascript files for this room, and if no javascript code is made available specifically for this roomID (HubID), the Injection Server returns code for generic rooms (You control what gets sent here too).  

**STEP 1: GET THE MOZILLA HUBS CLIENT**. 

You can either: 
- Follow the official way of doing things (fresh clone or fork from the official repository) at : https://hubs.mozilla.com/docs/hubs-cloud-custom-clients.html. 
- Use an already cloned Hubs Client because you already are developing in Hubs.   


**STEP 2: VALIDATE THE WORKING BRANCH**.    
Double check the working branch, usually, for a repository cloned from Mozilla, the working branch should be **hubs-cloud** .  

**STEP 3: SET UP THE ENVIRONMENT FILE**.  
Exact same process as for the Injection Server, add a .env file to the root of the project. 

![Screen_Shot_2021-05-24_at_4.30.11_PM](https://gitlab.com/sat-mtl/satellite/hubs-injection-server/-/wikis/uploads/a615b18ca8d69fdf78cb948abda82e06/Screen_Shot_2021-05-24_at_4.30.11_PM.png)

And proceed to add the following line (variable). This variable will be used by our fetch method in order to reach our Injection Server.    

**LOCAL ENVIRONMENT SETUP** 
When working with no self-signed certificates:   
```
INJECTION_URL = http://localhost:8557
```
or using the local host name previously configured:  
```
INJECTION_URL = http://api.hubs.local:8557
CORS_PROXY_SERVER="hubs-proxy.com"
```


** AWS ENVIRONMENT SETUP **
A standard setup on a Hubs Cloud server running on AWS could look like this: 

```
INJECTION_URL = https://yourInjectionServerSubDomain.yourHubsCloudDomain
```
Beware of trailing port numbers when deploying on AWS or on other cloud solutions. Port numbers are usually only present when working on local environments.  

**STEP 4: WEBPACK CONFIGURATION**. 

Now, we'll have to specify to the Hubs Client webpack server that we want to include our INJECTION_URL into NodeJS process.env variables so that we can access their values from within the code.  
- Open up the webpack.config.js file located at the root of the Mozilla Hubs Client repository.  
- Scroll to the bottom of the file where webpack's DefinePlugin is instanciated. 
- Add the following line to the argument list:  **INJECTION_URL: process.env.INJECTION_URL**
- The code should now look something like this:  

```
new webpack.DefinePlugin({
    "process.env": JSON.stringify({
    NODE_ENV: argv.mode,
    SHORTLINK_DOMAIN: process.env.SHORTLINK_DOMAIN,
    RETICULUM_SERVER: process.env.RETICULUM_SERVER,
    RETICULUM_SOCKET_SERVER: process.env.RETICULUM_SOCKET_SERVER,
    THUMBNAIL_SERVER: process.env.THUMBNAIL_SERVER,
    CORS_PROXY_SERVER: process.env.CORS_PROXY_SERVER,
    NON_CORS_PROXY_DOMAINS: process.env.NON_CORS_PROXY_DOMAINS,
    BUILD_VERSION: process.env.BUILD_VERSION,
    SENTRY_DSN: process.env.SENTRY_DSN,
    GA_TRACKING_ID: process.env.GA_TRACKING_ID,
    POSTGREST_SERVER: process.env.POSTGREST_SERVER,
    APP_CONFIG: appConfig,
    INJECTION_URL: process.env.INJECTION_URL
```



**STEP 4: MODIFYING src/hub.js**.  

In order to fetch code from the Injection server, we first need to be positive that the scene has loaded all of the assets it needs to display properly and then we can call the server.  We do this by adding a method inside the constant method called **connectToScene** . 

At the end of the connectToScene function, go ahead and add the following bits:  

```
const injectJS = () => {
    try {
    const myHub = hub.hub_id;
    //get the current hub_id and construct a url
    const url = `${process.env.INJECTION_URL}/hubs/libs/${myHub}`;
    console.log(url);
    //fetch the url with a get method which will return scripts to inject
    fetch(url, {
        method: "GET",
        mode: "cors",
        credentials: "include",
        headers: {
        "hubs-header": "yourHubsHeaderKey",
        "Content-Type": "application/json"
        },
        crossDomain: true
    })
        .then(body => {
        return body.text();
        })
        .then(data => {
        const urls = JSON.parse(data);
        let i = 0;
        let script, src;
        const body = document.querySelector("body");
        for (i = 0; i < urls.length; ++i) {
            console.log(`injecting ${urls[i].url}`);
            if (urls[i].url) {
            script = document.createElement("script");
            script.type = "text/javascript";
            src = document.createAttribute("src");
            src.value = urls[i].url;
            script.setAttributeNode(src);
            body.appendChild(script);
            }
        }
        })
        .catch(err => {
        console.warn(`ERROR ON FETCH ${err}`);
        });
    } catch (err) {
    console.warn(err);
    }
};
injectJS();
```

Be careful, the body of injectJS function should be at end of the connectToScene function. Indentation might be a bit tricky so pay attention that it is positioned inside connectToScene and not outside. 

For clarity's sake, the whole connectToScene function should look something like this: 

```
const connectToScene = async () => {
    let adapter = "janus";

    try {
      // Meta endpoint exists only on dialog
      await fetch(`https://${hub.host}:${hub.port}/meta`);
      adapter = "dialog";
    } catch (e) {
      // Ignore, set to janus.
    }

    scene.setAttribute("networked-scene", {
      room: hub.hub_id,
      serverURL: `wss://${hub.host}:${hub.port}`,
      debug: !!isDebug,
      adapter
    });

    while (!scene.components["networked-scene"] || !scene.components["networked-scene"].data) await nextTick();

    scene.addEventListener("adapter-ready", ({ detail: adapter }) => {
      let newHostPollInterval = null;

      // When reconnecting, update the server URL if necessary
      adapter.setReconnectionListeners(
        () => {
          if (newHostPollInterval) return;

          newHostPollInterval = setInterval(async () => {
            const currentServerURL = NAF.connection.adapter.serverUrl;
            const { host, port, turn } = await hubChannel.getHost();
            const newServerURL = `wss://${host}:${port}`;

            setupPeerConnectionConfig(adapter, host, turn);

            if (currentServerURL !== newServerURL) {
              console.log("Connecting to new Janus server " + newServerURL);
              scene.setAttribute("networked-scene", { serverURL: newServerURL });
              adapter.serverUrl = newServerURL;
            }
          }, 1000);
        },
        () => {
          clearInterval(newHostPollInterval);
          newHostPollInterval = null;
        },
        null
      );

      const sendViaPhoenix = reliable => (clientId, dataType, data) => {
        const payload = { dataType, data };

        if (clientId) {
          payload.clientId = clientId;
        }

        const isOpen = hubChannel.channel.socket.connectionState() === "open";

        if (isOpen || reliable) {
          const hasFirstSync =
            payload.dataType === "um" ? payload.data.d.find(r => r.isFirstSync) : payload.data.isFirstSync;

          if (hasFirstSync) {
            if (isOpen) {
              hubChannel.channel.push("naf", payload);
            } else {
              // Memory is re-used, so make a copy
              hubChannel.channel.push("naf", AFRAME.utils.clone(payload));
            }
          } else {
            // Optimization: Strip isFirstSync and send payload as a string to reduce server parsing.
            // The server will not parse messages without isFirstSync keys when sent to the nafr event.
            //
            // The client must assume any payload that does not have a isFirstSync key is not a first sync.
            const nafrPayload = AFRAME.utils.clone(payload);
            if (nafrPayload.dataType === "um") {
              for (let i = 0; i < nafrPayload.data.d.length; i++) {
                delete nafrPayload.data.d[i].isFirstSync;
              }
            } else {
              delete nafrPayload.data.isFirstSync;
            }

            hubChannel.channel.push("nafr", { naf: JSON.stringify(nafrPayload) });
          }
        }
      };

      adapter.reliableTransport = sendViaPhoenix(true);
      adapter.unreliableTransport = sendViaPhoenix(false);
    });

    const loadEnvironmentAndConnect = () => {
      updateEnvironmentForHub(hub, entryManager);
      function onConnectionError() {
        console.error("Unknown error occurred while attempting to connect to networked scene.");
        remountUI({ roomUnavailableReason: "connect_error" });
        entryManager.exitScene();
      }

      const connectionErrorTimeout = setTimeout(onConnectionError, 90000);
      scene.components["networked-scene"]
        .connect()
        .then(() => {
          clearTimeout(connectionErrorTimeout);
          scene.emit("didConnectToNetworkedScene");
        })
        .catch(connectError => {
          clearTimeout(connectionErrorTimeout);
          // hacky until we get return codes
          const isFull = connectError.msg && connectError.msg.match(/\bfull\b/i);
          console.error(connectError);
          remountUI({ roomUnavailableReason: isFull ? "full" : "connect_error" });
          entryManager.exitScene();

          return;
        });
    };

    window.APP.hub = hub;
    updateUIForHub(hub, hubChannel);
    scene.emit("hub_updated", { hub });

    if (!isEmbed) {
      loadEnvironmentAndConnect();
    } else {
      remountUI({
        onPreloadLoadClicked: () => {
          hubChannel.allowNAFTraffic(true);
          remountUI({ showPreload: false });
          loadEnvironmentAndConnect();
        }
      });
    }

    const injectJS = () => {
      try {
        const myHub = hub.hub_id;
        //get the current hub_id and construct a url
        const url = `${process.env.INJECTION_URL}/hubs/libs/${myHub}`;
        console.log(url);
        //fetch the url with a get method which will return scripts to inject
        fetch(url, {
          method: "GET",
          mode: "cors",
          credentials: "include",
          headers: {
            "hubs-header": "yourHubsHeaderKey",
            "Content-Type": "application/json"
          },
          crossDomain: true
        })
          .then(body => {
            return body.text();
          })
          .then(data => {
            const urls = JSON.parse(data);
            let i = 0;
            let script, src;
            const body = document.querySelector("body");
            for (i = 0; i < urls.length; ++i) {
              console.log(`injecting ${urls[i].url}`);
              if (urls[i].url) {
                script = document.createElement("script");
                script.type = "text/javascript";
                src = document.createAttribute("src");
                src.value = urls[i].url;
                script.setAttributeNode(src);
                body.appendChild(script);
              }
            }
          })
          .catch(err => {
            console.warn(`ERROR ON FETCH ${err}`);
          });
      } catch (err) {
        console.warn(err);
      }
    };
    injectJS();
  };
```

Now save the file, we are done for this part.  


| Previous Page | Home | Next Page |
| ------ | ------ | ------ |
| [Setting up the Hubs-Injection-Server.](./SettingUpTheHubsInjectionServer.md) | [index](../README.md) | [Setting Up Moziila Hubs Admin Options](./SettingUpAdvancedHubsCloudAdminOptions.md ) |

