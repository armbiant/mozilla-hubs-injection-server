# 5. HOW TO CREATE INJECTED COMPONENTS  
Before diving into the details of components creation, we take for granted you have basic understanding of threeJS and on how A-Frame components are setup.  If you have no clue on what I am talking about it might be a good idea to at least catch up some reading on those two topics:  
- [ThreeJS](https://threejs.org/)
- [A-Frame](https://aframe.io/)  
Reading up on those will help you greatly in understanding the inner workings of the visual aspects inside of Mozilla hubs.  

**STEP 1: CREATING THE RECTANGLE-TEXT COMPONENT FILES**.  
Our first very basic exemple will consist of a rectangle with a text label inside that we will inject inside a Hubs Room.  
At this point, it's important to understand that there are no difference between a regular Mozilla Hubs Component and an injected Hubs Component besides that one is built inside the Mozilla Hubs Client and the other one is loaded at runtime inside the DOM.  

- In the Injection Server project, go to static/hubs-components and create a new folder. Call it `rectangle`. Inside that folder, create a javascript file called `RectangleComponent.js`. 

- Then, go to your room `configs.js` (could be in the generic room) and link up `RectangleComponent.js` as such:  
```
{
    "modules": {
        "client":
        [
            {"id":"satutils", "url":"/lib/satutils.js"},
            {"id":"utils", "url":"/lib/utils.js"},
            {"id":"rectangleExemple", "url":"/hubs-components/rectangle/RectangleComponent.js"}
        ],
        "server":
        [
            
        ]
    }  
}
``` 
- Save all of those. 

**STEP 2: CREATING THE RECTANGLE-TEXT COMPONENT**.  
Now that everything is linked up, we can go ahead and start coding.  We will show you how to do this using JS Classes, but you can use your own style of coding.  
Most components creation require three basic steps:  
- Registering your component to the AFrame framework.  
- Create an HTML compnent template to house your component logic and link it's schema to A-Frame.
- Create an HTML entity and "embed" your component inside of it by using HTML attributes.  
- Copy and paste the following inside `RectangleComponent.js`.  
```

class RectangleComponent {
    constructor(rectangleID, rectangleConfigs) {
        this.rectangleID = rectangleID;
        this.defaultConfigs =  {
            text_string:"DEFAULT TEXT",
            rectangle_width:2.5,
            rectangle_height:0.4,
            rectangle_color:'#550000',
            rectangle_position:'0 2 0',
        }

        this.configs = {...this.defaultConfigs, ...rectangleConfigs};
        this.registerComponent(this.configs);
        this.buildTemplate("rectangle-media");
        this.createEntity();
    }

    registerComponent(configs) {
        AFRAME.registerComponent('rectanglecomponent', {
            schema: {
                rectangle_text_string: { type:'string', default:configs.text_string },
                rectangle_color: { type: 'color', default: configs.rectangle_color },
                rectangle_id:{type:'string', default: this.rectangleID},
                rectangle_width:{type:'number', default: configs.rectangle_width },
                rectangle_height:{type:'number', default: configs.rectangle_height },
                rectangle_position:{type:'string', default:configs.rectangle_position},
            },
            init:function() {
                let data = this.data;
                let el = this.el;
                this.update = this.update.bind(this);
                this.geometry = new THREE.PlaneBufferGeometry(data.rectangle_width, data.rectangle_height, 1, 1);
                this.material = new THREE.MeshBasicMaterial({ color: data.rectangle_color });
                this.material.transparent = true;
                this.material.opacity = 0.8;
                this.material.side = THREE.DoubleSide;
                
                // Create mesh.
                this.mesh = new THREE.Mesh(this.geometry, this.material);
                // Set mesh on entity.
                el.setObject3D('mesh', this.mesh);
                el.setAttribute('position', `${data.rectangle_position}`);
                el.setAttribute('text', 'value', `${data.rectangle_text_string}`);
                el.setAttribute('text', 'align', 'center');
                el.setAttribute('text', 'transparent', 'false');
                el.setAttribute('text', 'wrapCount', 30);
                el.setAttribute('text', 'width', 2.5);
            }
        });
    }
    buildTemplate(templateID) {
        let assets = document.querySelector("a-assets");
        let newTemplate = document.createElement("template");
        
        newTemplate.id = `${templateID}`;
        newTemplate.innerHTML = `<a-entity 
        class="interactable" 
        is-remote-hover-target 
        set-unowned-body-kinematic 
        tags="isHandCollisionTarget: false; isHoldable: false; offersHandConstraint: false; offersRemoteConstraint: false; inspectable: true; singleActionButton:true; isStatic: true;togglesHoveredActionSet: true;" 
        body-helper="type: static; mass: 1; collisionFilterGroup: 1; collisionFilterMask: 15;"
        hoverable-visuals 
        matrix-auto-update
        pinnable
        rectanglecomponent
        >
        </a-entity>`;
        assets.appendChild(newTemplate);
        NAF.schemas.add({
            // template to add (created above)
            template: `#${templateID}`,
            components: [{
                    component: "position"
                },
                {
                    component: "rotation"
                },
                {
                    component: "scale"
                },
            ]
        });
    }

    createEntity() {
        return new Promise((resolve, reject) => { 
            let doCreate = async () => { 
                let rectangleEntity = document.createElement("a-entity");
                let loadedRectangle = new Promise((resolve, reject) => { rectangleEntity.addEventListener('loaded', resolve, { once: true }) });
                rectangleEntity.setAttribute("id", `${this.rectangleID}Entity`);
                rectangleEntity.setAttribute("rectanglecomponent", `rectangle_id:${this.rectangleID};`);
                document.querySelector('a-scene').appendChild(rectangleEntity);
                await loadedRectangle;
                
                return resolve(loadedRectangle);
            }
            doCreate();
        });
    }
}

let rectangle;
setTimeout(()=> {
    rectangle = new RectangleComponent("rectangle1", null);
}, 
1000);

```
Save this and go back to your hubs room and reload the page, you should get see a red rectangle component inside your room with a text displaying : `DEFAULT TEXT`. 

If we take a few moments to look at the code, you will see that we first setup some configurations for our rectangle inside our constructor. This means that you can pass your own configuration whenever you instantiate a new RectangleComponent.  In this particular case, you can set the following values:  

- text_string
- rectangle_width
- rectangle_height 
- rectangle_color 
- rectangle_position 

Keep in mind that each rectangle you will create needs a unique ID. The ID is passed as the first parameter to the constructor. Second, and optionnal parameter is the options or configs object for the rectangle.  

Then, we build our component by calling three methods subsequently. 

**1.registerComponent(configs)**  
Thats where the backbone of our component gets built and registered with a-frame. We need to define it's schema properties and it's Javascript logic. Once properly registered, schema properties become accessible via HTML attributes. Remember that one, it took us a while to register that part.  

Inside the schema definition object, we set the actual attributes of our component and assing them with a type and a default value. Default values passed here are "inherited" from the RectangleComponent class instance config object.  

For the moment, we will only be using the init function of our A-Frame component. The init method gets called, you probably guessed it, on initialization. Whenever a new html entity that embeds a component is added inside the DOM, the init function is called. That's where we add our initial visuals and or behaviours, so, for this exemple, we create the rectangle, we assign it with the specified size, position and color. Once that's drawn, we add an A-Frame / Hubs "magical" text on top of it and give it some properties along with the text value we want to display.  

Pay attention to name your components properly when registrating A-Frame components. We used the name `rectanglecomponent`. As in: `AFRAME.registerComponent('rectanglecomponent',`  

**2.buildTemplate(templateID)**  

This is where we create an reusable html entity template that houses our component in HTML attribute form.  
The things to pay attention to here are:  
- A-Frame component template inside of Mozilla Hubs is whole lot easier when your template Id is suffixed with `-media`. Unless you know what you are doing, name all of your Hubs components as : `myComponentTemplateName-media`. 
- Take some time getting familiar with hubs tags and attributes such as:  
```
class="interactable" 
        is-remote-hover-target 
        set-unowned-body-kinematic 
        tags="isHandCollisionTarget: false; isHoldable: false; offersHandConstraint: false; offersRemoteConstraint: false; inspectable: true; singleActionButton:true; isStatic: true;togglesHoveredActionSet: true;" 
        body-helper="type: static; mass: 1; collisionFilterGroup: 1; collisionFilterMask: 15;"
        hoverable-visuals 
        matrix-auto-update
        pinnable
```  

- Always add your registered component in the html entity attributes of your template otherwise it will not take form. In our case, have a look at the last line of attributes being passed on to the template where we added the `rectanglecomponent`:  
```
class="interactable" 
        is-remote-hover-target 
        set-unowned-body-kinematic 
        tags="isHandCollisionTarget: false; isHoldable: false; offersHandConstraint: false; offersRemoteConstraint: false; inspectable: true; singleActionButton:true; isStatic: true;togglesHoveredActionSet: true;" 
        body-helper="type: static; mass: 1; collisionFilterGroup: 1; collisionFilterMask: 15;"
        hoverable-visuals 
        matrix-auto-update
        pinnable
        rectanglecomponent
        >
        </a-entity>`;
```   
- Append your newly created DOM element called newTemplate to the A-Frame assets list after it's creation. `assets.appendChild(newTemplate);`
- Then add your template Ids and it's sub-components as a schema to NAF (Networked A-Frame).  
```
NAF.schemas.add({
            // template to add (created above)
            template: `#${templateID}`,
            components: [
                {
                    component: "position"
                },
                {
                    component: "rotation"
                },
                {
                    component: "scale"
                },
            ]
        });
```

**3.createEntity()**
Here is where we place the last piece of the puzzle by creating:  
- an a-frame entity inside the DOM,   
- set one of it's attributes as our rectanglecomponent component  
- then and add it to Hub's A-Frame scene.    
- wait for it to be loaded and resolve the Promise.  

Pay attention to where we set an attribute as rectanglecomponent and the stringified object that follows. We are repassing the rectangle Id for no particular reason. It's just a way to show you how to bypass the default properties inferring logic we have in place. You could set all the attributes of our rectangularcomponent directly from there like in what follows:  

`rectangleEntity.setAttribute("rectanglecomponent", `rectangle_id:${this.rectangleID}; text_string:"HEY YOU"`;);`

You can pass any of the component attributes directly when you insert it into the DOM.  
Keep that in mind as we will be using this later.  


| Previous Page | Home | Next Page |
| ------ | ------ | ------ |
| [How to start development with the Hubs-Injection-Server.](./StartDevelopment.md ) | [index](../README.md) | [How to share components on the network.](./NetworkedComponents.md) |