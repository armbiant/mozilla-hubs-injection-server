const Mongoose  = require('mongoose')
let Schema = Mongoose.Schema;

Mongoose.Promise = Promise;

let HubComponent = new Schema({
        hubID:String,
        componentID:String,
        configs:Object
    });

module.exports = Mongoose.model('HubComponent', HubComponent);