'use strict';

const FS        = require('fs')

class DiskUtils {
    static MKDIR(path) {
        return new Promise((resolve, reject) => {
            const doTask = async() => {
                try {
                    await FS.promises.mkdir(path);
                    return resolve();
                } catch(err) {
                    if (err.code !== 'EEXIST') return reject(err);
                    return resolve();
                }  
            };
            doTask();
        });
    } 

    static FS_READ_FILE(path, encoding="utf8") {
        return new Promise((resolve, reject) => {
            FS.readFile(path, encoding, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }

    static FS_WRITE_FILE(path, fromData) {
        return new Promise((resolve, reject) => {
            FS.writeFile(path, fromData, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    }

    static FILE_EXISTS(path) {
        return new Promise((resolve, reject) => {
            FS.access(path, FS.F_OK, (err) => {
                if (err) 
                    return resolve(false);
                
                return resolve(true);
            });
        });
    }
}

module.exports = DiskUtils;