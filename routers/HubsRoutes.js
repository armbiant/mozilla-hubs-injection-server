const bodyParser        = require('body-parser')
    , express           = require('express')
    , router            = express.Router()
    , DiskUtils         = require('../utils/DiskUtils')
    , fs                = require('fs').promises
    , Hotloader         = require('../utils/HotLoader')
    , HubComponent      = require('../dbModels/HubComponent')
    , mergeFiles        = require('merge-files')
    , Path              = require('path');

require('dotenv').config();

class HubsRoutes {
    #componentsBasePath = Path.join(process.cwd(), "/static/hubs-components/");
    #roomsBasePath = Path.join(process.cwd(), "/static/rooms/");
    #basePath = Path.join(process.cwd(), "/static/");
    #counter = 0;
    #configsMap = new Map();

    constructor(configs, app) {
        this.router = express.Router();
        this.router.use(bodyParser.json());
        this.router.use(bodyParser.urlencoded({ extended: true }));

        this.app = app;

        this.router.get('/libs/:hubID', async (req, res, next) => {
            try {
                let hubID           = req.params.hubID;
                console.log(`Getting js files from /libs/${hubID}`);
                let configs         = await this.checkForHubConfigs(hubID);
                let clientFiles     = [];
                // console.log(`configs ${hubID}`);
                // console.log(configs);
                let clientFilesArray = configs.modules.client;
                if(this.#counter > 2) {
                    this.#counter = 0;
                }
                else {
                    this.#counter++;
                }
                
                let fileMerged = 'fileMerged'+this.#counter+'.js';
                let inputFiles = [];

                for (let i=0; i<clientFilesArray.length; ++i) {
                    // clientFiles.push({'id':clientFilesArray[i].id, 'url':process.env.BASE_URI+clientFilesArray[i].url});
                   inputFiles.push(process.cwd() + '/static/'+clientFilesArray[i].url);
                }
                //console.log(inputFiles);
                const status = await mergeFiles(inputFiles, 'static/injection/'+ fileMerged);
                res.send([{'id':fileMerged, 'url': process.env.BASE_URI+'/injection/'+fileMerged }]);
                //res.send(clientFiles);
            }
            catch(err) {
                console.error(err);
            }
        });
        
        this.router.get('/component/:hubID/:componentID', async (req, res, next) => {
            try {
                const doQuery = async()=> {
                    let params = req.params;
                    let comp = await HubComponent.findOne({'hubID':params.hubID, 'componentID':params.componentID}).lean(true);
                    res.send(JSON.stringify(comp));
                };
                doQuery();
            } catch(err) {
                res.status(500).send("Could not fetch component !");
            }
        });

        this.router.get('/room-components/:hubID', async (req, res, next) => {
            try {
                const doQuery = async()=> {
                    let params = req.params;
                    let comp = await HubComponent.find({'hubID':params.hubID});
                    res.send(comp);
                };
                doQuery();
            } catch(err) {
                res.status(500).send("Could not fetch room components !");
            }
        });

        this.router.post('/component', async (req, res, next) => {
            try {
                const doQuery = async()=> {
                    let params = req.body;
                    let comp = {
                        'hubID':params.hubID,
                        'componentID':params.componentID,
                        'configs':params.configs
                    };

                    let foundObj = await HubComponent.findOne({'hubID':comp.hubID, 'componentID':comp.componentID});
                    if (foundObj !== null && foundObj.componentID !== null) {
                        // console.log("updateOne", comp);
                        await HubComponent.updateOne({'hubID':comp.hubID, 'componentID':comp.componentID}, comp);
                    } else {
                        // console.log("createOne", comp);
                        let myComponent = new HubComponent(comp);
                        await myComponent.save();
                    }
                    res.send(comp);
                }
                doQuery();
            }
            catch(err) {
                res.status(500).send("Could not update or create component !");
            }
        });
        
        this.router.delete('/component', async (req, res, next) => {
            try {
                let doQuery = async()=> {
                        let params = req.body;
                        let comp = {
                            'hubID':params.hubID,
                            'componentID':params.componentID,
                        };

                        await HubComponent.findOneAndDelete({'hubID':comp.hubID, 'componentID':comp.componentID});
                        res.send();
                    }
                    doQuery();
                }
                catch(err) {
                    res.status(500).send("Could not delete component !");
                }
        });
    }

    checkFileOrFolder(path) {
        return new Promise((resolve, reject) => {
            const doCheck = async()=> {
                let exists = await DiskUtils.FILE_EXISTS(path);
                return resolve(exists);
            };
            doCheck();
        })
    }

    checkForHubConfigs(hubID) {
        return new Promise((resolve, reject) => {
            const doCheck = async()=> {
                try {
                    const path          = this.#roomsBasePath+hubID;
                    const configsPath   = path+"/configs.json";
        
                    if (!this.#configsMap.has(hubID)) {
                        let hubFolderExists = await this.checkFileOrFolder(path);
                        let configFileExists = await this.checkFileOrFolder(configsPath);
                        let jsonFile;
    
                        if (!hubFolderExists || !configFileExists) {
                            jsonFile = await Hotloader.loadJSON("/static/rooms/generic/configs.json");
                            this.#configsMap.set(hubID, jsonFile);
                            return resolve(jsonFile);
                        }
    
                        jsonFile = await Hotloader.loadJSON("/static/rooms/"+hubID+"/configs.json");
                        this.#configsMap.set(hubID, jsonFile);
                        return resolve(jsonFile);
                    } else {
                        return resolve(this.#configsMap.get(hubID));
                    }
                } catch(err) {
                    return reject(err);
                }
            }
            doCheck();
        });
    }
}

module.exports = HubsRoutes;